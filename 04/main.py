import os

with open('data.txt', 'r') as f:
    data=[i.strip() for i in f.readlines()]

def part1():
    res=0
    for line in data:
        ints = line.split(',')
        vals=[]
        for int1 in ints:
            vals.append([int(i) for i in int1.split('-')])
        # int2 = [int(i) for i in int2.split('-')] 
        vals.sort(key=lambda x : x[0])
        if vals[0][0]==vals[1][0]:
             res+=1
        elif vals[0][0]<=vals[1][0]:
            if vals[0][1]>=vals[1][1]:
                res+=1

    return res

def part2():
    res=0
    for line in data:
        ints = line.split(',')
        vals=[]
        for int1 in ints:
            vals.append([int(i) for i in int1.split('-')])
        # int2 = [int(i) for i in int2.split('-')] 
        vals.sort(key=lambda x : x[0])
        if vals[0][0]==vals[1][0]:
             res+=1
        elif vals[0][0]<=vals[1][0]:
            if vals[0][1]>=vals[1][0]:
                res+=1

    return res
if __name__=='__main__':
    print(part1())
    print(part2())