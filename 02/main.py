import os

def main():
# data=None
	abc="ABC"
	xyz="XYZ"
	amap=dict(zip(abc, list(range(1, len(abc)+1))))
	xmap=dict(zip(xyz, list(range(1, len(xyz)+1))))
	strat=dict(zip(xyz, list(range(0, 3*len(xyz)+1, 3))))

	with open(os.path.join(os.environ['BASE_AOC_DIR'], '02/data.txt'),'r') as f:
		data=[i.strip().split() for i in f.readlines()]

	total_score=0
	for i, j in data:
		total_score+=strat[j]
		if j=='Y':
			total_score+=amap[i]
		elif j=='X':
			if i=='A':
				done='Z'
			elif i=='B':
				done='X'
			else:
				done='Y'
			total_score+=xmap[done]
			
		else:
			if i=='A':
				done='Y'
			elif i=='B':
				done='Z'
			else:
				done='X'
			total_score+=xmap[done]

		# if amap[i]==xmap[j]:
		# 	total_score+=3
		# elif (i=='A' and j=='Y') or (i=='B' and j=='Z') or (i=='C' and j=='X'):
		# 	total_score+=6
		# total_score+=xmap[j]


	print("Total ", total_score)



if __name__ == "__main__":
	main()
