import os
from pathlib import Path

def find_priority(p):
		if p>=97:
			return (p-96)
		else:
			return (p-64)+26
		

class Day3():
	def __init__(self) -> None:
		data_path=os.path.join(Path(__file__).parent.resolve(), 'data.txt')
		with open(data_path,'r') as f:
			self.data=[i.strip() for i in f.readlines()]
	
	def part1(self):
		total_priority=0
		for i in self.data:
			common=set(i[:len(i)//2]).intersection(i[len(i)//2:])
			assert len(common)==1
			p=ord(list(common)[0])
			total_priority+=find_priority(p)

		return total_priority

	def part2(self):
		total_priority=0
		for i in range(0,len(self.data)//3):
			common=set(self.data[i*3]).intersection(self.data[i*3+1]).intersection(self.data[i*3+2])
			assert len(common)==1
			p=ord(list(common)[0])
			total_priority+=find_priority(p)
		return total_priority
	
	def main(self):
		print("Part 1", self.part1())
		print("Part 2", self.part2())

if __name__ == "__main__":
	Day3().main()