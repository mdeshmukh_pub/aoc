import os
from collections import deque
from pathlib import Path
import re

#                 [V]     [C]     [M]
# [V]     [J]     [N]     [H]     [V]
# [R] [F] [N]     [W]     [Z]     [N]
# [H] [R] [D]     [Q] [M] [L]     [B]
# [B] [C] [H] [V] [R] [C] [G]     [R]
# [G] [G] [F] [S] [D] [H] [B] [R] [S]
# [D] [N] [S] [D] [H] [G] [J] [J] [G]
# [W] [J] [L] [J] [S] [P] [F] [S] [L]
#  1   2   3   4   5   6   7   8   9 

stacks=[deque() for _ in range(10)]
contents=[
    'WDGBHRV',
    'JNGCRF',
    'LSFHDNJ',
    'JDSV',
    'SHDRQWNV',
    'PGHCM',
    'FJBGLZHC',
    'SJR',
    'LGSRBNVM'
]

for i, word in enumerate(contents):
    for letter in word:
        stacks[i].append(letter)

data_path=os.path.join(Path(__file__).parent.resolve(), 'data.txt')
with open(data_path, 'r') as f:
    data=[i.strip() for i in f.readlines()]

def part1():
    res=0
    for line in data:
        n, src, dst= [int(i) for i in re.findall('[0-9]+', line)]
        src, dst = src-1, dst-1
        for i in range(n):
            tmp=stacks[src].pop()
            stacks[dst].append(tmp)
    res=""
    for i in stacks:
        if len(i):
            res+=(i[-1])
    return res

def part2():
    stacks=[list(i) for i in contents]
    res=0
    for line in data:
        n, src, dst= [int(i) for i in re.findall('[0-9]+', line)]
        src, dst = src-1, dst-1
       
        tmp=list(stacks[src][(len(stacks[src])-n):])
        stacks[src]=stacks[src][:(len(stacks[src])-n)]
        stacks[dst]+=tmp
    res=""
    for i in stacks:
        if len(i):
            res+=(i[-1])
    return res
if __name__=='__main__':
    print(part1())
    print(part2())