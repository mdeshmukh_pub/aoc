
def main():
	current, max_cals=0,0
	line=True
	arr=[]
	with open('data.txt','r') as f:
		while line:
			if line!=True:
				if len(line.strip()):
					current+=int(line.strip())
				else:
					arr.append(current)
					max_cals=max(current, max_cals)
					current=0
			line=f.readline()
	if len(line.strip()):
		current+=int(line.strip())
	max_cals=max(current, max_cals)
	print("Top 1 ", max_cals)
	print("Top 3 ", sum(sorted(arr, reverse=True)[:3]))




if __name__ == "__main__":
	main()
